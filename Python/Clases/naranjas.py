#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 13:10:46 2020

@author: eduardo
"""


class naranjas():
    """
    tamaño # Diametro en cm
    nivel_de_acidez # pH 1-7
    color
    
    nivel_de_dulzura # 1-7
    madurez #
    """
    def __init__(self, size, NivelAC,
                 Color):
        self.tamaño = size
        self.nivel_de_acidez = NivelAC
        self.color = Color
    
    def __str__(self):
        return 'Diametro {} acidez {} color {}'.format(self.tamaño,
                                                       self.nivel_de_acidez,
                                                       self.color) 
    