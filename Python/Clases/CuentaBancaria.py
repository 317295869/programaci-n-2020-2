#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 14:06:41 2020

@author: eduardo
"""


class cuenta_bancaria():
    def __unit__(self,nombre,numero,monto_apertura = 0):
        self.saldo = monto_apertura
        self.saldo_minimo = 100
        self.tipo = "debito" # "credito"
        self.limite_de_credito = 0
        self.tasa_interes = 0.0
        self.manejo_cta = 500
        self.servicios_adicionales = {"seguros":{"auto":0, "vida":0},
                                      "creditos":{"auto":0 , "hipotecario":0}}
        self.ncta = numero
        self.usuario = nombre
    def deposito(self, monto):
        self.saldo += monto
    def saldo(self):
        return(self.saldo)
    def transferencia(self, cuenta_destino, monto):
        if self.saldo >= monto:
            self.deposito(-monto)
            cuenta_destino.deposito(monto)
        else:
            print("Saldo insuficiente")
    
    def retiro(self,monto):
        if self.saldo >= monto:
            self.deposito(-monto)
            return monto
        else:
            return 0
        
    def cerrar(self):
        self.retiro(self.saldo)


c1 = cuenta_bancaria()
c2 = cuenta_bancaria()

c1.deposito(150)
print(c1.saldo)

c1.transferencia(c2, 1500)
c1.transferencia(c2, 100)
print(c1.saldo)
print(c2.saldo)
