#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 12 13:13:42 2020

@author: eduardo
"""


#Escribir una función que reciba como entrada 10 números ingresados por el usuario
#esta función tiene que devolver una lista con el cuadrado de los números 
#que ingresó el usuario, no se puede utilizar ninguna función predefinida en 
#Python para elevar al cuadrado, además de que se debe verificar que cada
#que cada cadena ingresada es un número, de no ser  así mandar un mensaje
#"La cadena ingresada no es número"
# Hint: Utilizar función isnumeric

def powNumeros():
    L = []
    for i in range(10):
        N = int(input("Ingrese un numero:\n"))
        N = N*N
        L.append(N)
    print(L)
powNumeros()
    
#Escribir una función en python que verifique si el número es perfecto o no lo es
#Ejemplo: 6 con divisores positivos propios 1,2 y 3, 1+2+3 = 6, de manera equivalente
#Es igual a la mitad de la suma de todos sus divisores positivos 
#(1+2+3+6)/2 = 6. 

def PerfectNum():
    N = int(input("Ingrese un numero:\n"))
    L = []
    for i in range(1, N+1):
        if N%i == 0:
            L.append(i)
    count = 0
    for i in L:
        count += i
    if N == count/2:
        return print("El numero que ingresaste es un numero perfecto")
    else:
        return print("El numero que ingresaste no es un numero perfecro")
PerfectNum()
