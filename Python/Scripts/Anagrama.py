#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 13:05:15 2020

@author: eduardo
"""


# Anagrama Pedro Poder, Quieren Enrique
# Crear un programa en Python que dadas dos cadenas diga si
# es o no es un anagrama

Palabra1 = input('Ingresa una palabra:\n')
Palabra2 = input('Ingresa otra palabra:\n')
def anagrama(Palabra1, Palabra2):
    
    lista1 = Palabra1.lower()
    lista2 = Palabra2.lower()
    
    vocal_con = ['á','é', 'í', 'ó', 'ú'," "]
    vocal_sin = ['a', 'e', 'i', 'o', 'u',""]
    
    for vocal in vocal_con:
        lista1 = lista1.replace(vocal, vocal_sin[vocal_con.index(vocal)])
        lista2 = lista2.replace(vocal, vocal_sin[vocal_con.index(vocal)])
    
    Lista1 = list(lista1)
    Lista2 = list(lista2)
    
    
    Lista1.sort()
    Lista2.sort()

    cont = 0
    similar = True

    while cont < len(Palabra1) and similar:
        
        if Lista1[cont]==Lista2[cont]:
            cont = cont + 1
            print('Es un anagrama')
            
        else:
            similar = False
            print('No es un anagrama')
            
        break

    return similar

anagrama(Palabra1, Palabra2)