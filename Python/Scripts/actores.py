#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 13:06:57 2020

@author: eduardo
"""


#Dado el siguiente diccionario, define una función que te diga 
#la suma total del sueldo de los actores, define una segunda función que te diga
#que actor participó en más películas y por último una función que te diga
#la edad promedio de los actores.

actores = {
    "Juanito":{
            "NumPeliculas": 10,
            "SueldoMensual": 15000,
            "Edad": 25
        },
    "Pedro":{
            "NumPeliculas": 4,
            "SueldoMensual": 55000,
            "Edad": 35
        },
"Sandra":{
            "NumPeliculas": 28,
            "SueldoMensual": 25000,
            "Edad": 19
        },
    "Tina":{
            "NumPeliculas": 9,
            "SueldoMensual": 45000,
            "Edad": 45
        } 
    }

def suma_sueldo():
    cont = 0
    for i in actores:
        diccionario = actores[i]
        cont = cont + diccionario["SueldoMensual"]
    return cont
        
print("El suma de los sueldos de los actores es {0}". format(suma_sueldo()))

"""
def sueldoTotal(actores):
    sumaSueldos = 0
    for actor in actores:
        sumaSueldos += actores[actor]["SueldoMensual"]
    print(sumaSueldos)
"""

def mas_peliculas():
    cont = 0
    cadena = ""
    for i in actores:
        diccionario = actores[i]
        if cont < diccionario["NumPeliculas"]:
            cont = diccionario["NumPeliculas"]
            cadena = str(i)
    return cadena

print("El actor que ha participado en mas peliculas es {0}". format(mas_peliculas()))

"""
def masActuaciones(actores):
    masPeliculas = 0
    actorConMasPeliculas = ""
    for actor in actores:
        if actores[actor]["NumPeliculas"] > masPeliculas:
            masPeliculas = actores[actor]["NumPeliculas"]
            actorConMasPeliculas = actor
    print(actorConMAsPeliculas)
"""

def promEdad():
    cont = 0
    cont1 = 0
    for i in actores:
        diccionario = actores[i]
        cont += diccionario["Edad"]
        cont1 += 1
        cont2 = cont/ cont1
    return cont2
print("El promedio de las edades de los actores es {0}".format(promEdad()))

"""
def edadPromedio(actores):
    edades = []
    for actor in actores:
        edades.append(actores[actor]["Edad"])
    edadPromedio = sum(edades)/len(edades)
    print(edadPromedio)
"""