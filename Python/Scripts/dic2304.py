#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 13:07:16 2020

@author: eduardo
"""

"""
1) Implementa un programa que almacene un diccionario con los
créditos de las materias de un semestre la key tiene que 
ser el nombre de la materia y el valor asignado el número 
de créditos, después se debe mostrar el siguiente mensaje 
"(Nombre de la materia) tiene (Número de créditos) ". #Hint: 
Puede servir el método dict.items() (busquénlo en google)
"""

creditos = {'Calculo': 18, 'Algebra': 10, 'Geometria':10, 'Programacion':10}

for i in creditos.items():
    print(i[0], "tiene", i[1], "creditos")

print()
    
"""
2) Cuenta el número de ocurrencias de palabras en el siguiente texto dado:
(Deben Utilizar un diccionario para almacenar las palabras y el número de 
veces que aparecieron)

"Este es un texto de prueba, queremos utilizar este texto para hacer una prueba de nuestro programa"
"""

Texto = "Este es un texto de prueba, queremos utilizar este texto para hacer una prueba de nuestro programa"

texto = Texto.split()

Texto = Texto.replace(",", " ")

palabras = {}

for i in texto:
    palabras[i] = 0
    
for i in texto:
    if  i in palabras:
        palabras[i] += 1

print(palabras)
print()

"""
3) Muestra los productos con el precio menor o igual al que introduce el usuario.
(Debes utilizar un diccionario para almacenar los productos)
"""

"""
Productos:

Computadora $500
Televisión $300
Radio $400
Teclado $500
Mouse $100
Ventilador $200
"""

Productos = {'una computadora': 500, 'una television': 300, 'una radio': 400, 'un teclado': 500, 'un mouse': 100, 'un ventilador': 200}

Valor = float(input("Introduce la cantidad de dinero con la que cuentas:\n"))

for i in Productos.items():
    if(i[1] <= Valor):
        print("Puedes comprar", i[0])
    else:
        print("No te alcanza para ningun producto")
        
        break
        