#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

BAsado en el página https://devcode.la/tutoriales/diccionarios-en-python/

"""
def separa():
    print("-"*20)

#%%
s = """
Para definir un diccionario, se encierra el listado de valores entre llaves. 
Las parejas de clave y valor se separan con comas, 
y la clave y el valor se separan con dos puntos.
"""
print(s)
diccionario = {'nombre' : 'Carlos', 'edad' : 22, 'cursos': ['Python','Django','JavaScript'] }
separa()

#%%
s = """
Podemos acceder al elemento de un Diccionario mediante la clave de este elemento,
 como veremos a continuación:
"""
print(s)
print(diccionario['nombre']) #Carlos
print(diccionario['edad'])#22
print(diccionario['cursos'] )#['Python','Django','JavaScript']
separa()

#%%
s = """
También es posible insertar una lista dentro de un diccionario. 
Para acceder a cada uno de los cursos usamos los índices:
"""
print(s)
print(diccionario['cursos'][0])#Python
print(diccionario['cursos'][1])#Django
print(diccionario['cursos'][2])#JavaScript
separa()

#%%
s = """
Para recorrer todo el Diccionario, podemos hacer uso de la estructura for:
"""
print(s)
for key in diccionario:
  print(key, ":", diccionario[key])
separa()  

#%%
s = """
Métodos de los Diccionarios
"""

s+="""
dict ()
Recibe como parámetro una representación de un diccionario y si es factible, devuelve un diccionario de datos.
"""
print(s)
dic =  dict(nombre='nestor', apellido='Plasencia', edad=22)
print(dic)
separa()

#%%
s = """
zip()

Recibe como parámetro dos elementos iterables, ya sea una cadena, una lista o una tupla. Ambos parámetros deben tener el mismo número de elementos. Se devolverá un diccionario relacionando el elemento i-esimo de cada uno de los iterables.
"""
print(s)
dic = dict(zip('abcd',[1,2,3,4]))
print(dic)
separa()

#%%
s = """
items()

Devuelve una lista de tuplas, cada tupla se compone de dos elementos: el primero será la clave y el segundo, su valor.
"""
print(s)
dic =   {'a' : 1, 'b' : 2, 'c' : 3 , 'd' : 4}
items = dic.items()
print(items)
separa()

#%%
s = """
keys()

Retorna una lista de elementos, los cuales serán las claves de nuestro diccionario.
"""
print(s)
dic =  {'a' : 1, 'b' : 2, 'c' : 3 , 'd' : 4}
keys= dic.keys()
print(keys)
separa()

#%%
s = """
values()

Retorna una lista de elementos, que serán los valores de nuestro diccionario.
"""
print(s)
dic =  {'a' : 1, 'b' : 2, 'c' : 3 , 'd' : 4}
values= dic.values()
print(values)
separa()

#%%
s = """
clear()

Elimina todos los ítems del diccionario dejándolo vacío.
"""
print(s)
dic1 =  {'a' : 1, 'b' : 2, 'c' : 3 , 'd' : 4}
dic1.clear()
print(dic1)
separa()

#%%
s = """
copy()

Retorna una copia del diccionario original.
"""
print(s)
dic = {'a' : 1, 'b' : 2, 'c' : 3 , 'd' : 4}
dic1 = dic.copy()
print(dic1)
separa()

#%%
s = """
fromkeys()

Recibe como parámetros un iterable y un valor, devolviendo un diccionario que contiene como claves los elementos del iterable con el mismo valor ingresado. Si el valor no es ingresado, devolverá none para todas las claves.
"""
print(s)
dic = dict.fromkeys(['a','b','c','d'],1)
print(dic)
separa()

#%%
s = """
get()

Recibe como parámetro una clave, devuelve el valor de la clave. Si no lo encuentra, devuelve un objeto none.
"""
print(s)
dic = {'a' : 1, 'b' : 2, 'c' : 3 , 'd' : 4}
valor = dic.get('b') 
print(valor)
print(dic)
separa()

#%%
s = """
pop()

Recibe como parámetro una clave, elimina esta y devuelve su valor. Si no lo encuentra, devuelve error.
"""
dic = {'a' : 1, 'b' : 2, 'c' : 3 , 'd' : 4}
valor = dic.pop('b') 
print(valor)
print(dic)
separa()

#%%
s = """
setdefault()

Funciona de dos formas. En la primera como get
"""
print(s)
dic = {'a' : 1, 'b' : 2, 'c' : 3 , 'd' : 4}
valor = dic.setdefault('c')
print(valor)
s = """
Y en la segunda forma, nos sirve para agregar un nuevo elemento a nuestro diccionario.
"""
print(s)
valor = dic.setdefault('e',5)
print(dic)
separa()

#%%
s = """
update()

Recibe como parámetro otro diccionario. Si se tienen claves iguales, actualiza el valor de la clave repetida; si no hay claves iguales, este par clave-valor es agregado al diccionario.
"""
print(s)
dic_1 = {'a' : 1, 'b' : 2, 'c' : 3 , 'd' : 4}
dic_2 = {'c' : 6, 'b' : 5, 'e' : 9 , 'f' : 10}
dic_1.update(dic_2)
print(dic_1)
separa()

