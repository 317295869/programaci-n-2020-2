#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 28 23:50:59 2020

@author: eduardo
"""

# Función contarVocales(cadena) Regresa el número de vocales almacenadas en cadena.

cadena = str(input("Ingresa una palabra:\n"))

def contarVocales(cadena):
    count = 0
    for i in cadena:
        if i in "AEIOUÄÖÜaeiouäöü":
            count = count + 1
    return count
print("El numero de vocales es:", contarVocales(cadena))

# Función contarConsonantes(cadena) Regresa el número de consonantes almacenadas
# en cadena.

def contarConsonantes(cadena):
    count = 0
    for i in cadena:
        if i not in "AEIOUÄÖÜÁÉÍÓaeiouáéóäöü":
            count = count + 1
    return count
print("El numero de consonantes es:", contarConsonantes(cadena))

# Función contarAcentos(cadena) Regresa el número de vocales acentuadas 
# almacenadas en cadena.

def contarAcentos(cadena):
    count = 0
    for i in cadena:
        if i in "ÁÉÍÓÚáéíóú":
            count = count + 1
    return count
print("El numero de acentos es:", contarAcentos(cadena))

# Función contarMayusculas(cadena) Regresa el número de mayúsculas almacenadas 
# en cadena.

def contarMayusculas(cadena):
    count = 0
    for i in cadena:
        if i in "AÁÄBCDEÉËFGHIÍÏJKLMNOÓÖPQRSTUÜÚVWXYZ":
            count = count + 1
        return count
print("El número de mayusculas es:", contarMayusculas(cadena))

# Función contarMinusculas(cadena) Regresa el número de minusculas almacenadas 
# en cadena.

def contarMinusculas(cadena):
    count = 0
    for i in cadena:
        if i in "aáäbcdeéëfghiíïjklmnoóöpqrstuúüvwxyz":
            count = count + 1
        return count
print("El numero de minusculas es:", contarMinusculas(cadena))

# Función palíndromo(cadena). Un palíndromo es una frase que se le igual de 
# izquierda a derecha que de derecha a izquierda. Por ejemplo, "Oir a Dario" o 
# "Anita lava la tina". La función regresa True si cadena contiene un palíndromo
# y False si no lo es.

def palindromo(cadena):
    cadenatmp = cadena.replace(" ", "")
    i= 0
    j= len(cadenatmp) -1
    while (i <=j):
        if cadenatmp[i] != cadenatmp[j]:
            return False
        i +=1
        j -= 1
    return True
print(palindromo(cadena))
