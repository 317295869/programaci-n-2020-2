#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 28 19:32:33 2020

@author: luis
"""
nombre_archivo ='/home/eduardo/Documentos/programaci-n-2020-2/Python/Scripts/GyH.sbv'
with open(nombre_archivo) as archivo:
    mensaje = []
    todos = []
    for linea in archivo:
        if linea != '\n':
            mensaje.append(linea.rstrip())
        else:
            todos.append(mensaje)            
            mensaje = []
    datos = {}    
    for mensaje in todos:
        [nombre, *texto] = mensaje[1].split(":")
        if not nombre in datos:
            datos[nombre] = []
        datos[nombre].append(" ".join(texto))
        
for alumno in datos:
        print("_"*30)
        print(datos[alumno])

print("="*20)
for alumno in datos:
    if len(datos[alumno])== 10:
        print(datos[alumno][3])
      
#%%
#"Ruta y nombre de archivo en Windows"
#nombre_archivo ='c:\\Users\\luis\\Documentos\\programaci-n-2020-2\\Python\Scripts\Programación2020-05-29-11_06.sbv'
"Ruta y nombre de archivo en Linux"
nombre_archivo ='/home/eduardo/Documentos/programaci-n-2020-2/Python/Scripts/GyH.sbv'
"""
Algunas extenciones de arhivos de texto plano:
    sbv, srt, csv, xml, py, c, 
    cpp, java, ini, bat, js, 
    html, css, tex, bat
"""
with open(nombre_archivo) as f:
        for line in f:
            print(line, end = "")
"""
¿Cuántos alumnos contestaron las 10 preguntas?
¿Cual es la fruta que más gusta ?
¿Cual es el color que más gusta ?
¿Cual es el alimento que más gusta?
¿Cual es el alimento que menos gusta?
¿Cuantos alumnos se levantan antes de las 7 de la mañana?
¿Cuantos alumnos se levantan después de las 7 de la mañana y antes de las 11:00 de la mañana ?
"""
            
#Primero importamos regex
import re
#¿Cuántos alumnos contestaron las 10 preguntas?
#de esta linea podemos observar que 40 alumnos contestaron las 10 preguntas
for alumno in datos:
    if len(datos[alumno]) == 10:
        print("Contestaron las 10 preguntas {}".format((str(len(datos)))))
        break
#¿Cual es la fruta que más gusta ?
#esto se encuentra en alumno[0]

p = "Mango"
t = open(nombre_archivo).read()

m = re.findall(p, t)

print("La fruta que más gusta es el mango con " + str(len(m)) + " estudiantes")

#¿Cual es el color que más gusta ?

p = "Azul"
t = open(nombre_archivo).read()

m = re.findall(p, t)

print("El color que más gusta es el azul con " + str(len(m)) + " estudiantes")

#¿Cual es el alimento que más gusta?

p = "Tacos"
t = open(nombre_archivo).read()

m = re.findall(p, t)

print("La comida que más gusta son los tacos con " + str(len(m)) + " estudiantes")

#¿Cual es el alimento que menos gusta?

p = "Hígado encebollado"
t = open(nombre_archivo).read()

m = re.findall(p, t)

print("La comida que menos gusta es el higado encebollado con " + str(len(m)) + " estudiantes")

# En esta parte se tuvoq que hacer unos ajustes para que todo cuadrará
#¿Cuantos alumnos se levantan antes de las 7 de la mañana?

p = "6:40:00|6:30:00"
t = open(nombre_archivo).read()

m = re.findall(p, t)

print("Las personas que se levantan antes de las 7 de la mañana son " + str(len(m)) + " estudiantes")

#¿Cuantos alumnos se levantan después de las 7 de la mañana y antes de las 11:00 de la mañana ?

p = "7:00:00|8:00:00|9:30:00|9:00:00|9:20:00|8:30:00|10:00:00"
t = open(nombre_archivo).read()

m = re.findall(p, t)

print("Las personas que se levantan entre las 7 y 11 de la mañana son " + str(len(m)) + " estudiantes")

#Bibliografía:
#    https://robologs.net/2019/05/05/como-utilizar-expresiones-regulares-regex-en-python/

#%%
"""
índice, minuto, dato, hora
 , 10, nombre, 3:17.353
0, 15, fruta, 8:34.380
1, 20, color, 13:28.206
2, 25, Lugar de nacimiento, 18:46.275
3, 30, bachillerato, 24:43.692
4, 35, platillo favorito, 28:51.877
5, 40, platillo no favorito, 35:15.960
6, 45, carrera, 39:45.466
7, 50, alcaldía/municípo, 45:12.589
8, 55, hora despertar para ir a la facultad, 50:37.852
9, 60, hora despertar contingencia, 52:49.780
"""

