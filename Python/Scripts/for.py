09nov#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 19:13:37 2020

@author: luis
"""
#utilidades.py debe estar en el directorio Scripts
from utilidades import espera, agregaRutas

agregaRutas(['/Documentos/programaci-n-2020-2/Python/Scripts/', 
             '/Documentos/programaci-n-2020-2/Python/Scripts/26022020/'],
"home")

cadena = "Esta es una cadena no muy larga"
for caracter in cadena:
    print(caracter)
espera()
    
materias = ["Álgebra II", "Cálculo II", "Programación"]
for materia in materias:
    print(materia)
espera()    

carrera = [["Álgebra I", "Cálculo I", "THC"], materias,[]]
for semestre in carrera:
    print(semestre)
espera()
    ##
edades = [17, 18 ,19, 20, 21]
for edad in edades:
    print(edad)
espera()
    
edades.append(40)
for edad in edades:
    print(edad, end=" años ")
print()
espera()
    
tuplaEdades = (17, 18 ,19, 20, 21)

FacultadDeCiencias=[["Actuaría",[["Primer semestre"],
                                 ["Segundo semestre"],
                                 ["Tercer semestre"],
                                 ["Cuarto semestre"],
                                 ["Quinto semestre"],
                                 ["Sexto semestre"],
                                 ["Septimo semestre"],
                                 ["Octavo semestre"]
                                 ]],
                    ["Biología",[]],
                    ["Ciencias de la Computación",[]],
                    ["Ciencias de la Tierra",[]],
                    ["Física",[]],
                    ["Física Biomédica",[]],
                    ["Matemáticas",[]],
                    ["Matemáticas aplicadas",[["Primer semestre", ["Algebra Superior I",9, "Calculo I",7, "Geometria Analitica I",10, "IMD",8, "THC",8]],
                                 ["Segundo semestre", ["Algebra Superior II",10, "Calculo II", 10, "Geometria Analitica II",10, "Programación",10, "Ciencia Basica",10]],
                                 ["Tercer semestre", ["Algebra Lineal I",10, "Calculo II",10, "Probabilidad I",10, "Manejo de Datos",10, "Taller de Modelacion I",10]],
                                 ["Cuarto semestre", ["Algebra Lineal II",10, "Calculo IV",10, "Probabilidad II",10, "Ecuaciones Diferenciales I",10, "Investigacion de Operaciones",10]],
                                 ["Quinto semestre", ["Analisis Matematico I",10, "Variable Compleja I",10, "Analisis Numerico",10, "Inferencia Estadistica",10, "Taller de Modelacion II",10]],
                                 ["Sexto semestre", ["Analisis Matematico Aplicado",10, "Sistemas Dinamicos no Lineales",10, "Procesos Estocasticos I",10, "Optativa",10, "Optativa Bloque I",10]],
                                 ["Septimo semestre", ["Ecuaciones Diferenciales Paraciales I",10, "Formacion Cientifica I",10, "Optativa1",10, "Optativa",10]],
                                 ["Octavo semestre", ["Formacion Cientifica II",10, "Proyecto II",10, "Optativa",10, "Optativa",10, "Optativa",10]]
                                 ]],
                    ]
UNAM = [["Ciudada Unversitaria",
         ["Facultad de Arquitectura", ["Arquitectura", "Arquitectura del Paisaje", "Diseño Industrial", "Urbanismo"],
          ["Facultad de Ciencias"], 
          ["Facultad de Ciencias Politicas y Sociales", ["Antropologia", "Ciencias de la Comunicacion", "Ciencias Politicas y Administracion Publica", "Relaciones Internacionales", "Sociologia"]],
          ["Facultad de Contaduria y Administración", ["Administracion", "Contaduria", "Informatica", "Negocios Internacionales"]],
          ["Facultad de Derecho", ["Derecho"]],
          ["Facultad de Economia", ["Economia"]],
          ["Facultad de Filosofia y Letras", ["Bibliotecologia y Estudios de la Informacion", "Desarrollo y Gestion Interculturales", "Estudios Latinoamericanos", "Filosofia", "Geografia", "Historia", "Lengua y Literaturas Hispanicas", "Lengua y Literaturas Modernas(Letras Alemanas, Francesas, Inglesas, Italianas o Portuguesas)", "Letras Clasicas", "Literatura Dramatica y Teatro", "Pedagogia"]],
          ["Facultad de Ingenieria", ["Ingenieria",["Ambiental", "Civil", "de Minas y Metalurgia", "Electrica Electronica", "en Computacion", "en Sistemas Biomedicos", "en Telecomunicaciones", "Geofisica", "Geologica", "Geomatica", "Industrial", "Mecanica", "Mecatronica", "Petrolera"]]],
          ["Facultad de Medicina", ["Ciencia Forense", "Fisioterapia", "Investigacion Biomedica Basica", "Medico Cirujano", "Neurociencias"]],
          ["Facultad de Medicina Veterinaria y Zootecnia", ["Medicina Veterinaria y Zootecnia"]],
          ["Facultad de Odontologia", ["Cirujano Dentista"]],
          ["Facultad de Psicologia", ["Psicologia"]],
          ["Facultad de Quimica", ["Ingenieria Quimica", "Ingenieria Quimica Metalurgica", "Quimica", "Quimica de Alimentos", "Quimica e Ingenieria en Materiales", "Quimica Farmaceutica Biologica"]],
          ["Escuela Nacional de Artes Cinematograficas", ["Cinematografia"]],
          ["Escuela Nacional de Ciencias de la Tierra",["Ciencias de la Tierra", "Geografia Aplicada"]],
          ["ENALLT", ["Lingüistica Aplicada", "Traduccion"]],
          ["Escuela Nacional de Trabajo Social", ["Trabajo Social"]],
          ["Centro Universitario de Teatro", ["Teatro y Actuacion"]],
          ["Instituto de Investigaciones en Matematicas Aplicadas y en Sistemas", ["Ciencia de Datos"]]
          ]],
        ["Facultad de Artes y Diseño", ["Arte y Diseño", "Artes Visuales", "Diseño y Comunicacion Visual"]],
        ["FES Acatlan",["Actuaria", "Arquitectura", "Ciencias Politicas y Administracion Publica", "Comunicacion", "Derecho", "Diseño Grafico", "Economia", "Enseñanza de (Aleman)(Español)(Frances)(Ingles)(Italiano) Como Lengua Extranjera", "Enseñanza de Ingles", "Filosofia", "Historia", "Ingenieria Civil", "Lengua y Literaturas Hispanicas", "Matematicas Aplicadas y computacion", "Pedagogia", "Relaciones Internacionales", "Sociologia"]],
        ["FES Aragon",["Arquitectura", "Comunicacion y Periodismo", "Derecho", "Diseño Industrial", "Economia", "Ingenieria", ["Civil", "Electrica Electronica", "En Computacion", "Industrial", "Mecanica"], "Pedagogia", "Planificacion para el Desarrollo Agropecuario", "Relaciones Internacionales", "Sociologia"]],
        ["FES Cuatitlan",["Administracion", "Bioquimica Diagnostica", "Contaduria", "Diseño y Comunicacion Visual", "Farmacia", "Informatica", "Ingenieria", ["Agricola", "en Alimentos", "en Telecomnicaciones, Sistemas y Electronica", "Industrial", "Mecanica Electrica", "Quimica"], "Mediciana Veterinaria y Zootecnia", "Quimica", "Quimica Industrial", "Tecnologia"]],
        ["FES Iztacala", ["Biologia", "Cirujano Dentista", "Enfermeria", "Medico Cirujano", "Optometria", "Psicologia"]],
        ["FES Zaragoza", ["Biologia", "Cirujano Dentista", "Desarrollo Comunitario para el Envejecimiento", "Enfermeria", "Ingenieria Quimica", "Medico Cirujano", "Nutriologia", "Psicologia", "Quimica Farmaceutico Biologica"]],
        ["Facultad de Musica", ["Etnomusicologia", "Musica", ["Canto", "Composicion", "Educacion Musical", "Instrumentista", "Piano"]]],
        ["Escuela Nacional de Enfermeria y Obstetricia", ["Enfermeria", "Enfermeria y Obtetricia"]],
        ["ENES Juriquilla", ["Ciencias de la Tierra", "Ciencias Genomicas", "Ingenieria en Energias Renovables", "Negocios Internacionales", "Neurociencias", "Ortesis y Protesis", "Tecnologia"]],
        ["ENES Leon", ["Administracion Agropecuaria", "Ciencias Agrogenomicas", "Desarrollo Territorial", "Desarrollo y Gestion Interculturales", "Economia Industrial", "Fisioterapia", "Odontologia", "Optometria"]],
        ["ENES Merida", ["Ciencias Ambientales", "Ciencias de la Tierra", "Desarrollo y Gestion Interculturales", "Geografia Aplicada", "Manejo Sustentable de Zonas Costeras"]],
        ["ENES Morelia", ["Administracion de Archivos y Gestion Documetal", "Arte y Diseño", "Ciencia de Materiales Sustentables", "Ciencias Agrofosrestales", "Ciencias Ambientales", "Ecologia", "Estudios Sociales y Gestion Local", "Geociencias", "Geohistoria", "Historia del Arte", "Literatura Intercultural", "Musica y Tecnologia Artistica", "Tecnologias para la Informacion en Ciencias"]],
        ["Centro de Ciencias Genomicas", ["Ciencias Genomicas"]],
        ["Centro de Nanociencias y Nanotecnologia", ["Nanotecnología"]],
        ["Instituto en Energias Renovables",["Ingenieria en Energias Renovables"]],
        ["Sedes de la UNAM en el extranjero",
         ["UNAM",["San Antonio", "Chicago", "Los Angeles", "Seattle"]],
         ["Centro de Estudios Mexicanos",["UNAM-Tucson", "Canada", "UNAM-China", "UNAM-Costa Rica", "UNAM-Francia","UNAM-España", "UNAM-Reino Unido", "UNAM-Alemania", "UNAM-Boston", "UNAM-Sudafrica"]]]
         ]
espera()

try:
    tuplaEdades.append(40)
    tuplaEdades.pop()
except Exception as e:
    print("Error: "+ str(e))
    print("Error: {}".format(e))

