#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  8 13:45:43 2020

@author: eduardo
"""

"f(x) = 1 + 2x¹⁰⁰"
pl = [0]*101; pl[0] = 1; pl[100] = 2
pd = {0:1, 100:2}
pll = [[0,1], [100,2]]
plt = [(0,1), (100,2)]

"f(5)"
"1*(5)⁰ + 0*(5)¹ + 0*(5)² + ... + 0*(5)⁹⁹ + 2*(5)¹⁰⁰"

n = len(pl)
suma = 0
x = 1
for i in range(n):
    suma = suma + pl[i]*x**i

s = 0
for potencia in range(n):
    s += pl[potencia]*x**potencia
    
print(suma)
print(s)

def evalua_polinomio_lista(coeficientes, x):
    n = len(coeficientes)
    suma = 0
    for i in range(n):
        suma = suma + coeficientes[i]*x**i
    return suma

def evalua1_polinomio_lista(coeficientes, x):
    n = len(coeficientes)
    s = 0
    for potencia in range(n):
        s += pl[potencia]*x**potencia
    return s

def evalua2_polinomio_lista(coeficientes, x):
    return sum([ coeficientes[i]*x**i for i in range(len(coeficientes))  ] )
            
print(evalua_polinomio_lista(pl, 1))
print(evalua1_polinomio_lista(pl, 1))
print(evalua2_polinomio_lista(pl, 1))
#%%
"f(x) = 1 + 2x¹⁰⁰"
"g(x) = 2x² -3x¹⁰⁰ + 5"
"f(x) + g(x)"
pd = {0:1, 100:2}
pd1 = {0:5, 2:2, 100:3}
def evalua_polinomio_dic(polinomio, x):
    suma = 0
    for potencia in polinomio:
        suma = suma + polinomio[potencia]*x**potencia
    return suma

print(evalua_polinomio_dic(pd, 1))

def suma_polinomios(polinomio1, polinomio2):
    resultado = {}
    for coeficiente in polinomio1:
        resultado[coeficiente]= 0
    for coeficiente in polinomio2:
        resultado[coeficiente]= 0
    for coeficiente in polinomio1:
        resultado[coeficiente] += polinomio1[coeficiente]
    for coeficiente in polinomio2:
        resultado[coeficiente] += polinomio2[coeficiente]
    polinomiofinal = dict(sorted(resultado.items()))
    return print(polinomiofinal)
suma_polinomios(pd, pd1)

#%%

# 1 + x² + 3x² - 5x² + 25 + x¹⁰⁰

pl = [ [0,1] , [2,1], [2,3], [2,-5], [0,25], [100,1]]

def simplifica(pl):
    r = {}
    for i in pl:
        if i[0] in r:
            r[i[0]] += i[1]
        else:
            r[i[0]] = i[1]
    return r
d = simplifica(pl)
print(d)

