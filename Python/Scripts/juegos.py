#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 01:19:32 2020

@author: eduardo
"""

# La computadora calcula un número al azar entre 5 y 27 y le pide al usuario adivinar el número

import random

n = 5
m = 27

# Función adivinaA(5,27). El programa finaliza hasta que el jugador adivine el 
# número e indica el número de intentos

def adivinaA(n,m):
    count = 0
    a = random.randint(n,m)
    while True:
        count += 1
        num = int(input("Introduce un numero:\n"))
        if num == a:
            print("Correcto!", count, "Intentos")
            break
        else:
            print("Intentalo nuevamente")
adivinaA(n,m)

# b) Función adivinaB(5,27). El programa finaliza hasta que el jugador adivine 
# el número e indica el número de valores antes del intervalo y el número de 
# valores después del intervalo

def adivinaB (n,m):
    a= random.randint(n,m)
    count1= 0
    count2= 0
    print(a)
    while True:
        num = int(input("Introduce un número:\n "))
        if num < n:
            count1 += 1
            print("Intentalo nuevamente")
        elif num > m:
            count2 += 1
            print("Intentalo nuevamente")
        elif num == a:
                print("Correcto! Adivinaste el número seleccionado, que era", a)
                print("valores antes del intervalo", count1)
                print("valores fuera del intervalo", count2)
                break
adivinaB(n,m)

# Función adivinaC(5,27).El programa finaliza hasta que el jugador adivine el 
# número. En cada intento fallido la computadora le indica al jugador si el 
# número que escribió está antes o después del intervalo

def adivinaC(n,m):
    a= random.randint(n,m)
    print(a)
    while True:
        num = int(input("Introduce un número:\n "))
        if num < n:
            print("Intentalo nuevamente!\n Tu número está antes del intervalo")
        elif num > m:
            print("Intentalo nuevamente!\n Tu número está despues del intervalo")
        elif n< num < m and num != a:
            print("Tu número está dentro del intervalo, pero no es el correcto\n Intenta otra vez")
        elif num == a:
            print("Correcto!, adivinaste el número")
            break
adivinaC(n, m)

# Función adivinaD(5,27). El programa finaliza hasta que el jugador adivine el 
# número. En cada intento fallido la computadora le indica al jugador si el 
# número que escribió está antes o después del intervalo. Al finalizar indica 
# el número de valores antes del intervalo y el número de valores después del 
# intervalo que escribió el jugador.

def adivinaD(n, m):
   a= random.randint(n,m)
   count1= 0
   count2= 0
   print(a)
   while True:
        num = int(input("Introduce un número:\n "))
        if num < n:
            count1 += 1
            print("Intentalo nuevamente!\n Tu número está anteso del intervalo")
        elif num > m:
            count2 += 1
            print("Intentalo nuevamente!\n Tu número está despues del intervalo")
        elif n< num < m and num != a:
            print("Tu número está dentro del intervalo, pero no es el correcto\n Intentalo nuevamente!")
        elif num == a:
                print("Correcto! Adivinaste el número que es", a)
                print("Veces que intentaste antes del intervalo ", count1)
                print("Veces que intentaste despues del intervalo ", count2)
                break
adivinaD(n, m)

# Función adivinaE(5,27). El programa finaliza hasta que el jugador adivine el 
# número. En cada intento fallido la computadora le indica al jugador si el 
# número que escribió es mayor, o menor, al número buscado. Al finalizar indica 
# el número de valores menores y el número de valores mayor que escribió el 
# jugador.

def adivinaE(n,m):
   a= random.randint(n,m)
   count1= 0
   count2= 0
   print(a)
   while True:
        num = int(input("Introduce un número:\n "))
        if num < a:
            count1 += 1
            print("Intentalo nuevamente!\n Escogiste un número menor al correcto")
        elif num > a:
            count2 += 1
            print("Intentalo nuevamente!\n Escogiste un número mayor al correcto")
        elif num == a:
                print("Correcto! El numero elegido es", a)
                print("Estas son las veces que intentaste antes del intervalo: ", count1)
                print("Estas las veces que intentaste despues del intervalo: ", count2)
                break
adivinaE(n, m)

# Función adivinaF(5,27). El programa finaliza hasta que el jugador adivine el 
# número. En cada intento fallido la computadora le indica al jugador si el 
# número que escribió es mayor, o menor, al número buscado. Al finalizar indica 
# el número de valores menores y el número de valores mayor que escribió el 
# jugador. La función regresa una lista con los valores que escribió el jugador 
# hasta que adivinó.

def adivinaF(n,m):
   a= random.randint(n,m)
   print(a)
   count = 0
   cad1 = 'Estos son los valores menores a '+str(a)+': '
   cad2 = 'Estos son los valores mayores a '+str(a)+': '
   while True:
        num = int(input("Introduce un número:\n"))
        if num < a:
            cad1 += str(num) + ", "
            count += 1
            print("Intentalo nuevamente!\n El numero que elegiste esta antes del numero correcto")
        elif num > a:
            cad2 += str(num) + ", "
            count += 1
            print("Intentalo nuevamente!\n El numero que escogiste esta despues del numero correcto")
        elif num == a:
                print("Correcto!\n Ese el nuemro que tenias que elegir")
                print(cad1)
                print(cad2)
                print("Tu numero de intentos fue de", count )
                break
adivinaF(n, m)

def adivina():
    a = random.choice(adivinaA(n,m), adivinaB(n,m), adivinaC(n,m), adivinaD(n,m), adivinaE(n,m), adivinaF(n,m))
    print(a)
    while True:
        break
adivina()


