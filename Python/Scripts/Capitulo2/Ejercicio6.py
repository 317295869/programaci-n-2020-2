#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 00:08:17 2020

@author: eduardo
"""

"""
State the main differences between identifiers and literals. Given a token in a
program, how does Python know whether it is an identifier or a literal?
"""

"""
Podemos decir que en principio los Identificadores son valores fijos, pero que
al ser evaluados reciben otro valor. Y que las literales son valores fijos
durante todo el proceso, a menos que se les asigne otro valor.
"""