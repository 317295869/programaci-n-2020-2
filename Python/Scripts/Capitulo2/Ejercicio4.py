#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 00:07:41 2020

@author: eduardo
"""

# Ejercicio 4 página 35 del libro "An Introduction to Python and Computer Programming"
"""
A car runs at a constant speed of 20 km/h. When it passes another case,
the latter starts to accelerate in order to catch up. Assuming that the first case
keeps a constant speed, and the second car keeps a constant acceleration of 2 m/s2.
"""
v= 20
a=2
v=v*1000/3600
print(v)
d=v**2/a
print(d)
t=d/v
print(t)
print()
"""
The annual interest rate of a savings account is 4.1 %. John has $10,000 in his account,
and aims at saving $50,000 within 5 years by depositing a fixed amount of money to his
account in the beginning of each year, including this year. How much money does John need
"""
A=10000*(1.041**5)
print(A)
i=(1.041**4)+(1.041**3)+(1.041**2)+(1.041)
print(i)
x=(50000-A)/i
print(x)
print()
"""
In a shooting exercise, a coach stands 5 m away from a trainee, and throws a target up
vertically at 5 m/s. If the trainee must fire her gun exactly 0.5 s after the throwing of the ball,
then at which angle should she aim? If she must fire the gun exactly 1 s after the throwing,
"""
vy=5
vx=5/0.5
print(vx)
from math import atan, sqrt
Ang1=atan(vy/vx)
print(Ang1)
vx=5/1
print(vx)
Ang2=atan(vy/vx)
print(Ang2)
print()
#  John deposited an initial sum of $3000 in his account. After 3 years, the balance reaches
AI=3000
AF=3335.8
D=AF-AI
print(D)
D_AL_AÑO=D/3
INT=D_AL_AÑO/100
print(INT)
print()
# The three sides of a triangle are 3, 4 and 6m, respectively. What is its area?
l1=3
l2=4
l3=6
s=(l1+l2+l3)/2
Area=sqrt(s*(s-l1)*(s-l2)*(s-l3))
print(Area)
