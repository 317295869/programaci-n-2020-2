#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 00:07:06 2020

@author: eduardo
"""

# Use IDLE to calculate the following mathematical values.

from math import pow, sqrt, log, pi, sin, factorial

a = pow(10, 5)
print(a)

b = sqrt(10)
print(b)

a1=1
b1=-7
c1=10
d1=(b1**2)-(4*a1*c1)
sol1=(-b1-sqrt(d1))/(2*a1)
sol2=(-b1+sqrt(d1))/(2*a1)
print('Las soluciones son {0} y {1}'.format(sol1,sol2))

d = log(2 + sqrt(5))
print(d)

Area_circulo=pi*5.5**2
print('El area del circulo de radio 5.5 es {0}'.format(Area_circulo))

f = sin(2.5)
print(f)

import cmath

a2=1
b2=-2
c2=10
d2 = (b2**2)-(4*a2*c2)
sol3=(-b1-cmath.sqrt(d2))/(2*a2)
sol4=(-b1+cmath.sqrt(d2))/(2*a2)
print('Las soluciones son {0} y {1}'.format(sol3,sol4))

h = factorial(4)
print(h)


def suma(n):
    r = 32
    for i in range(n+1):
        r+= i
    return(r)
    
print(suma(128))

n = 3
h = 1
while n <= 17:
    h *= n
    n += 1
print(h)

