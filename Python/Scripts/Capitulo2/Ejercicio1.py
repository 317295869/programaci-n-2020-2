#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 21:42:41 2020

@author: eduardo
"""

# What are the values of the following expressions?

a = 1 + 3 * 2 - 5 + 4
print(a)

b = 1 + 3 * (2 - 5) + 4
print(b)

c = 5 ** 2 ** 2 * 3 + 1
print(c)

d = 5 ** (2 ** 2) * 3 + 1
print(d)

e = 1 + 3/2
print(e)

f = 1 + 3.0/2
print(f)

g = - 2 - 1
print(g)

h = - (2 - 1)
print(h)

i = 3.0 + 3/2
print(i)

j = 3 + 3/2.0
print(j)

k = - 1 ** 0.5
print(k)