#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 00:07:59 2020

@author: eduardo
"""

"""
What are the three most important properties of a Python object? Which of them
can change after the object is constructed, if the object is a number?
"""

"""
Las tres cosas importantes de los objetos son los identificadores, 
valores de asignación y uso. Los objetos modificables una vez ya construidos 
son las listas, diccionarios y variables.
"""