#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 00:07:25 2020

@author: eduardo
"""

"""What are the values of the following binary numbers if they are (a) non-negative;
or (b) 2’s Complements?
"""
a = int('01001', 2)
print(a)

b = int('100', 2)
print(b)

c = int('1100', 2)
print(c)

d = int('11111', 2)
print(d)

e = int('11111111', 2)
print(e)