#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 13:12:32 2020

@author: eduardo
"""

print("Comenzamos")
for i in [0,1,2]:
    print("Hola", end="")
print()
print("Final")

list = [1,3,5,7,9]
for i in list:
    print(i)

palabra = input("Introduce la palabra a imprimir:")
for i in range(15):
    print(palabra)

edad = int(input("Ingresa tu edad:"))
for i in range(edad):
    print("Hasta hoy has cumplido"  +   str(i+1) +"años")

Asignaturas = ["Programacion", "Calculo", "Algebra"]
print(Asignaturas)
"""for i in Asignaturas:
    print(i)
"""