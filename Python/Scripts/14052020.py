#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 13:23:51 2020

@author: eduardo
"""


#Escribe una función de Python para verificar si una cadena es un pangrama o no.
"""Un pangrama es un texto que usa todas las letras del alfabeto de un idioma. 
"""
#"Tengo un libro de papiroflexia sobre las hazañas y aventuras de Don quijote de la Mancha en Kuwait."

O = input("ingresa una frase:\n ")

def Pangrama(O):
    lista = O.lower()
    
    vocal_con = ['á','é', 'í', 'ó', 'ú'," "]
    vocal_sin = ['a', 'e', 'i', 'o', 'u',""]
    
    for vocal in vocal_con:
        lista = lista.replace(vocal, vocal_sin[vocal_con.index(vocal)])
    abecedario = "abcdefghijklmnopqrstuvwxyz"
    Frase = ""
    for i in O:
       if i in abecedario:
           P = True
           Frase += i
           print('No es un pangrama')
       else:
           P = False
           print('Es un pangrama')
       break

    return P

Pangrama(O)
#%%
#Escribe una función en python que se llame triangulopascal que imprima
#las primeras n filas del triángulo de pascal

"""
El Triángulo se construye de la siguiente manera: escribimos el número 
«1» centrado en la parte superior; después, escribimos una serie de 
números «1» en las casillas situadas en sentido diagonal descendente, 
a ambos lados; sumamos las parejas de cifras situadas horizontalmente 
(1 + 1), y el resultado (2) lo escribimos debajo de dichas casillas; 
continuamos el proceso escribiendo en las casillas inferiores la suma de
las dos cifras situadas sobre ellas (1 + 2 = 3)... 

Ver más en Brainly.lat - https://brainly.lat/tarea/2413837#readmore
"""

def Pascal(n):
    lista = [[1],[1,1]]

    for i in range(1,n):
        linea = [1]

        for j in range(0,len(lista[i])-1):
            linea.extend([ lista[i][j] + lista[i][j+1] ])
        linea += [1]
        lista.append(linea)
        
    return print(lista)

Pascal(10)

def triangulo_Pascal(n):
    trow = [1]
    y = [0]
    for x in range(max(n,0)):
        print(trow)
        paresLista =zip(trow + y, y + trow)
        trowTemp = []
        for (l, r) in paresLista:
            trowTemp.append(l + r)
        trow = trowTemp
    return n >= 1
triangulo_Pascal(10)



 






