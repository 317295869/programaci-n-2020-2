#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 13:08:48 2020

@author: eduardo
"""



# Escribe un programa que sume todos los elementos de una lista.

L = []
def sumaLista(L):
    resultado = 0
    for i in L:
        resultado = i + resultado
    return resultado
sumaLista(L)
print(sumaLista([1,2,3]))

# Escribe un programa que multiplique todos los elementos de una lista.

def prodLista(L):
    resultado = 1
    for i in L:
        resultado = i * resultado
    return resultado
prodLista(L)
print(prodLista([1,2,3,4]))

#Escriba un programa de Python para obtener el mayor número de una lista.
M=[2,3,5,7,9,10]
def Maxnum(M):
    M.sort()
    a = M.pop()
    print(a)
Maxnum(M)
    
#Escriba un programa Python para contar el número de cadenas donde la longitud 
#de la cadena es 2 o más y el primer y el último carácter son los mismos de una 
#lista de cadenas dada.

N = ["ana", "oso", "ala", "loco", "dinosaurio", "Ajo", "mmmmmmmm"]

def misma_N(N):
    cont = 0
    for i in N:
        if len(i) >= 2 and i[0] == i[(len(i)-1)]:
            cont = cont + 1
        else:
            cont = cont
    return(cont)
    print(cont)
misma_N(N)

#Escribe un programa en python para ordenar una lista de tuplas 
#que se orden respecto al ultimo de sus elementos

O = [(2,5), (1,2), (4,4), (2,3), (2,1)]
O.sort(key=lambda O: O[1])
print(O)

#Escribe un programa de Python para eliminar duplicados de una lista

P = [2,3,5,7,8,7,6,5,4,2,7,9]
print(list(set(P)))

#Escribe un programa en Python que te diga si la lista es o no vacia
Q=[]
def Hole(Q):
    if Q:
        print("No vacía")
    else:
        print("vacia")

print(Hole([]))
print(Hole([1,2,3]))