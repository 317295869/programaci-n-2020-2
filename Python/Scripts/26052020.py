#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 26 13:22:24 2020

@author: eduardo
"""


# Sintaxis [[valor salida] for i in objeto iterable if (condiciones)]

# Elevar al cuadrado los elementos de una lista
ls1 = [5,10,15,20,25]
res = []
for i in ls1:
    res.append(i**2)
print(res)

# Comprension de listas elevar al cuadrado los elementos de una lista
res = [i**2 for i in [5,10,15,20,25]]
print(res)

# Elevar lps elementos pares de una lista
ls2 = [1,2,3,4,5,6,7]
res = []
for i in ls2:
    if i%2 == 0:
        res.append(i**2)
print(res)

# Comprension de listas elevar solo los numeros pares al cuadrado
res = [i**2 for i in ls2 if i%2 == 0]
print(res)

# En la ls2 elevar al cuadrado si el numero es par, de lo contrario elevar
# al cubo

res = []
for i in ls2:
    if i%2 == 0:
        res.append(i**2)
    else:
        res.append(i**3)
print(res)

# Utilixando comprension de listas
res = [[i**2 if i%2 == 0 else i**3 for i in ls2]]
print(res)

# Para cada numero en ls2 obten el numero y su posicion en ls1
# como una lista de tuplas. HInt: metodo index
ls1 = [9,3,6,1,5,0,8,2,4,7]
ls2 = [6,4,6,1,2,2]
#(6,2)
res = []
for i in ls2:
    res.append((i, ls1.index(i))) #list.index(elemento)
print(res)

res = [(i, ls1.index(i)) for i in ls2]
print(res)

# Para cada numero en ls2 obten el numero y su posicion en ls1
# como un diccionario. Mint: metodo index

res = {}
for i in ls2:
    res.update({i:ls1.index(i)})
print(res)

res = {}
res.update({i:ls1.index(i) for i in ls2})
print(res)
    