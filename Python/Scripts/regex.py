#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 13:12:12 2020

@author: eduardo
"""


import re
# Regex o expresion regular, es una secuencia de caracteres que forma un patron
# de busqueda

# [] cada que quieras  definir un conjunto de caracteres "permitidos" o un
# patron vamos a escribirlo por ejemplo [a-m]
txt = "Hola mundo"
x = re.findall("[a-m]", txt)
print(x)

# . implica que puede existir cualquier caracterer menos "newline"
# ho..la
txt = "hello h##o heiio he wrld"
x = re.findall("he..o", txt)
print(x)

# \ secuencia especial "\d"
txt = "Adrian cumplio 100 años"
x = re.findall("\d", txt)
print(x)

# ^ Empieza con cierta cadena, letra, letras, etcetera
# [^5] "empata" todo menos el 5 hola5.

txt = "Hola Mundo"
x = re.findall("^Hola", txt)
if x:
    print("La cadena comienza con Hola")
else:
    print("La cadena no comienza con Hola")


# $ Es para especificar con que patron termina la cadena

txt = "Hola Mundo"
x = re.findall("Mundo$", txt)
if x:
    print("La cadena termina con Mundo")
else:
    print("La cadena no termina con Mundo")

# + Una o mas ocurrencias

strr = "Ana compro una manzana y una bolsa"
x = re.findall("an+", strr)
print(x)
if x:
    print("Hay al menos un match")
else:
    print("No hay ningun match")

# * 0 o mas apariciones

strr = "Ana compro una manzana y una bolsa"
x = re.findall("an*", strr)
print(x)
if x:
    print("Hay al menos un match")
else:
    print("No hay ningun match")

# {} Especificamos exactamente el numero de ocurrencias de cierto patron
# la{2}

strs = "a Daniel no le gusta la comida salada all"
x = re.findall("al{1}", strs)
print(x)
if x:
    print("Hay un match")
else:
    print("No hay ningun match")

# | Uno u otro "hola|adios"
txt = "Te gustan las peras o las manzanas?"
x = re.findall("peras|manzanas", txt)
print(x)
if x:
    print("Hay al menos un match")
else:
    print("No hay ningun match")
