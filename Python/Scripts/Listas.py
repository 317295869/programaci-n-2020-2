#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 14:55:41 2020

@author: eduardo
"""

l1= [15, 23.6, "una cadena", "Otra cadena"]
l1.append("Fin")
print(l1)
print(l1.count(15))
x = 1
l1.insert(2,"aqui")
print(l1)
l1.insert(0,"inicio")
print(l1)
valor=l1.pop()
print(l1)
print(valor)
valor=l1.pop(len(l1)//2)
print(l1)
print(valor)

def swap(x,y):
    l=[y,x]
    return l
[a, b] = swap(10, -90)
print(a)
print(b)