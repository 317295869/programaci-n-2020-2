#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 21 13:13:45 2020

@author: eduardo
"""


# Escribe un program en Python que acepte una cadena y calcule el nemero
# de digitos y letras

cadena = str(input("Ingresa una cadena:\n"))
espacio = cadena.replace(" ", "")
letras = "abcdefghijklmnñopqrstuvwxyz"
numeros = "0123456789"
contl = 0
contn = 0
for i in cadena:
    if i in letras:
        contl += 1
    elif i in numeros:
        contn += 1
print("La longitud de la cadena es de {0} letras y {1} numeros".format(contl,contn) )


# Escribe un programa en Python para calcular la edad de un perro en los años
# del perro
n = int(input("Ingresa la edad de su perro:\n"))
def Edad_Perro(n):
    if n <= 12:
        return EdadPerro[n-1]

EdadPerro = [15,24,28,32,36,40,44,48,52,56,60,64]

print("La edad del perro es de {0}".format(Edad_Perro(n)))

# Primero versión general tamar cualquiera de las tres lista
# Cuanto pesa en kilos mi perro y la edad para calcular su edad en años de perrp

def Edad_PerroGral():
    Pq = [15,24,28,32,36,40,44,48,52,56,60,62]
    Md = [15,24,28,32,36,42,47,50,51,60,65,69]
    Gd = [15,24,28,32,36,45,50,55,61,66,72,77]
    Peso = float(input("¿Cuanto pesa su perro en kilogramos?\n"))
    Edad = int(input("¿Que edad tiene su perro en años de humano?\n"))
    if Peso <= 9:
        Edad = Pq[Edad - 1]
        Tamaño = "pequeño"
    elif Peso > 9 and Peso < 23:
        Edad = Md[Edad - 1]
        Tamaño = "mediano"
    else:
        Edad = Gd[Edad - 1]
        Tamaño = "grande"
    print("Tu perro es {0} y tiene {1} años de perro".format(Tamaño, Edad))
Edad_PerroGral()

""" Escribe un programa de Python para verificar la validez de una contraseña
(entrada de los usuarios).

Validacion:

Al menos una letra entre (a-z) y una letra entre [A-Z]
Al menos un numero entre (0-9)
Al menos un caracter especial de ($, #, @)
Longitud minima de 6 caracteres
Longitud maxima de 16 caracteres. """

def Contraseña():
    numeros = "0123456789"
    Letrasm = "abcdefghijklmnñopqrstuvwxyz"
    LetrasM = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
    caracteres = "$#@"
    n = True
    while n:
        contra = input("Ingrese su contraseña:\n")
        if len(contra) < 6 or len(contra) > 16:
            print("Contraseña no valida\n Su contraseña debe de contener entre 6 y 16 caracteres")
        else:
            cont1 = 0
            cont2 = 0
            for i in contra:
                if i in Letrasm:
                    cont1 += 1
                if i in LetrasM:
                    cont2 += 1
            if cont1 == 0 or cont2 == 0:
                print("Contraseña no valida\n La contraseña debe incluir minimo una letra minuscula y una letra mayuscula")
            else:
                cont3 = 0
                for i in contra:
                    if i in caracteres:
                        cont3 += 1
                if cont3 == 0:
                    print("Contraseña no valida\n Debe incluir minimo un caracter especial")
                else:
                    cont4 = 0
                    for i in contra:
                        if i in numeros:
                            cont4 += 1
                    if cont4 == 0:
                        print("Contraseña no valida\n La contraseña debe incluir minimo un numero alfanumerico")
                    if cont1 != 0 and cont2 != 0 and cont3 != 0 and cont4 != 0:
                        print("La contraseña si es valida")
Contraseña()