#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 13:06:18 2020

@author: eduardo
"""


dic = {'Nombre': 'Fulanito', 'Edad': 15, 'Materias': ['Matematicas','Fisica', 'Quimica']}
print(dic)
print()
print(dic['Nombre'])
print(dic['Edad'])
print(dic['Materias'])
print(dic['Materias'][0])
print(dic['Materias'][1])
print(dic['Materias'][2])
print()
dic2 = {"Nombre": 'Perenganito', 11: 'Edad'}
for i in dic2:
    print(i)
    print(dic2[i])
    
#Escribe un programa que pregunte su nombre, edad, direccion y
#telefono y lo guarde en un diccionario. Despues hay que mostra por pantalla
#el mensaje <nombre> tiene <edad> años, vive en <direccion> y su nuemro de
#telefono es <telefono>
"""
Nombre = str(input('¿Cual es su nombre?:\n'))
Edad = int(input('¿Cual es tu edad?:\n'))
Direccion = str(input('¿Cual es tu direccion?:\n'))
Telefono = int(input('¿Cual es tu numero de telefono?:\n'))

Usuario = {'nombre': Nombre, 'edad': Edad, 'direccion': Direccion, 'telefono': Telefono}
print(Usuario['nombre'], 'tiene',Usuario['edad'], 'años', 'vive en', Usuario['direccion'] ,'y su numero de telefono es', Usuario['telefono'])
"""
#Escribe un programa que guarde en un diccionario  los precios de la fruta de 
#la tabla, pregunte al usuario por  una fruta y una cantidad en kilos, muestra
#despues en pantalla el precio de ese numero de kilos de fruta. Si la fruta
#no se encuentra en el diccionario debes mostrar un mensaje "Nos se encuentra"

#Platano 10.50 pesos
#Manzana 15 pesos
#Pera 20 pesos
#Naranja 5 pesos

Frutas = {'Platano':10.50, 'Manzana': 15, 'Pera': 20, 'Naranja':5}
Fruta = input('Ingresa el nombre de una fruta\n')
Cantidad = float(input('Ingresa una cantidad de fruta en kilos\n'))
for i in Frutas:
    if i == Fruta:
        Total = Cantidad * Frutas[i]
        print("El precio de {0} es {1} pesos".format(Fruta, Total))
    else:
        print("La fruta no se encuentra en la tabla")
        
    break