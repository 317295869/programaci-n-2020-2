#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 11:45:06 2020

@author: luis
"""

def miRaiz(a, e = 0.0001):
    """
    Regresa una aproximación a la raíz cuadrada del número a.
    Se calcula construyendo rectángulos de área a cuyos lados 
    sean cada vez mas parecidos.
    Se considera una áproximación aceptable cuando la diferencia
    de las longitudes de los lados es menor al error de 0.001
    """
    #e = 0.001
    b = 1
    h = a
    while abs(b - h)>e:
        b = (b + h)/2
        h = a/b
    return b

if __name__ == "_main_":
    print(miRaiz(19))
    print(miRaiz(19, 0.00001))
    print(miRaiz(19, 0.000001))

def abs1(x):
    if x == 0:
        return x
    elif x < 0:
        x = -1 * x
        return x
    elif x > 0:
        return x

def abs2(x):
    if x == 0:
        return x
    elif x < 0:
        x = -1 * x
        return x
    else:
        return x
    
def abs3(x):
    resultado = 0
    if x >= 0:
        resultado = x
    else:
        resultado = -x
    return resultado

def mcd(a, b):
    q = a // b
    r = a - b*q
    while r != 0:
        a = b;
        b = r
        q = a // b
        r = a - b*q
    return b

def sigULAM(n): 
    if n % 2 == 0:
        resultado = n/2
    else:
        resultado = 3*n + 1
    return resultado

def ULAM1(n):
    while True:
        n = sigULAM(n)
        print(n)
        if n == 1:
            break

def MCD(a, b, *c):
    """
    Calcular el mcd de dos o más valores
    """
    resultado=mcd(a,b)
    for valor in c:
        resultado=mcd(resultado,valor)
    return resultado
"""
def mcdTradicional(a,b.c):
    """
    
"""
    Parameters
    ----------
    a : TYPE
        DESCRIPTION.
    b.c : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    

if __name__ == "_main_":
    print("mcd(24,36,40)={}".format(MCD(24,36,40)))
    print("mcd(24,36)={}".format(MCD(24,36)))
    print("mcd(24,36,...={}".format(MCD(24,36,40,921024,8,7,4,5)))

n = int(input("Ingrese el valor inicial:\n "))
def ULAM(n):
    while n>1:
        if n % 2 == 0:
            n = n/2
        else:
            n = 3*n + 1
        print(n)
    return n
ULAM(n)

def mcd2(c, d):
    resto = 0
    while (d > 0):
        resto = d
        d = c % d
        c = resto
    return c


if __name__== "__main_":
    print("mcd(24,36,40)={}".format(MCD(24,36,40)))
    print(mcd(85,25))
    
if __name__ == "_main_":
    print(sigULAM(6))
    print(sigULAM(3))
    
    print(mcd2(164, 72))
    
    print("Desde el modulo")
    print(__name__)
    
# 1.- Función suma(n) regresa la suma de los primeros n enteros iniciando por n 
# y terminando con 0. 
    
n = int(input("ingresa un número:\n"))
def suma(n):
    suma = 0
    for n in range(1,n+1):
        suma += n
        n -= 1
    print('La suma desde {0} hasta 1 es {1}'.format(n+1,suma))
suma(n)

# Función sumaPares(n) regresa la suma de los pares menores o iguales a n.

def sumaPares(n):
    suma = 0
    for n in range(1,n+1):
        if (n%2) == 0:
            suma += n
        n -= 1
    print('La suma de los numeros pares desde {0} hasta 2 es {1}'.format(n+1,suma))
sumaPares(n)

# Función sumaImpares(n) regresa la suma de los impares menores o iguales a n.

def sumaImpares(n):
    suma = 0
    for n in range(1,n+1):
        if (n%2) == 1:
            suma += n
        n  -= 1
    print('La suma de los numeros impares desde {0} hasta 1 es {1}'.format(n+1,suma))
sumaImpares(n)

# Función numeracion(n) muestra la numeración del 1 al n

def numeracion(n):
    for i in range(n):
        print(i+1)
numeracion(n)

# Función numeracionDecreciente(n) muestra la numeración del n al 1

def numeracionDecreciente(n):
    for i in range(n):
        n -= 1
        print(n+1)
numeracionDecreciente(n)



        

