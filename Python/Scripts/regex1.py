#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 13:16:31 2020

@author: eduardo
"""


import re
# re.match - primer patron encontrado y su(s) indice(s)
# re.search - true or false
# re.findall -lista
# ? una aparicion o ninguna. Ejemplo ca?r, cr, car

# Escribir un programa en python que tenga una a seguido de 0 o mas b's.
def empatatexto(text):
    patron = 'ab*'
    if re.search(patron, text):
        return ('Se encontro una coincidencia')
    else:
        return ('No se encontraron coincidencias')
print(empatatexto("ac"))
print(empatatexto("abc"))
print(empatatexto("abbc"))

# Escribir un programa en python que tenga una a seguido de 1 o mas b's.
def empatatexto(text):
    patron = 'ab+'
    # 'abb*'
    if re.search(patron, text):
        return ('Se encontro una coincidencia')
    else:
        return ('No se encontraron coincidencias')
print(empatatexto("ac"))
print(empatatexto("abc"))
print(empatatexto("abbc"))

# Escribir un programa en python que encuentre una cadena que tiene
# a seguido de 3 b's.
def empatatexto2(text):
    patron = 'ab{3}'
    if re.search(patron, text):
        return ('Se encontro una coincidencia')
    else:
        return ('No se encontraron coincidencias')
print(empatatexto2("ac"))
print(empatatexto2("abc"))
print(empatatexto2("aabbbbbc"))
print(empatatexto2("aabbb"))

# Escribir un programa para verificar que una cadena contenga
# solo un cierto conjunto de caracteres de a-z, A-Z, 0-9.

def carpermitido(string):
    string = re.search('[a-zA-Z0-9]', string)
    return bool (string)
print(carpermitido('ABCDEFabcdef123450'))
print(carpermitido("#$%^&!}{"))

# Escribe un programa de Python para encontrar secuencias de letras minusculas
# unidas por un guion bajo. Ejemplo aa_cbb

def carpermitido1(string):
    patron = '[a_z]'
    if re.search(patron, string):
        return ('Se encontro una coincidencia')
    else:
            return ('No se encontro una coincidencia')
print(carpermitido1('a_bcdef'))
print(carpermitido1('_A!bcdef'))
print(carpermitido1('0!bcdef'))
