#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 13:34:17 2020

@author: eduardo
"""

"""
a es el valor original al 
que necesito calcular el 
seno

b es el valor del ultimo
seno que evalue
"""

from math import sqrt
a = 0.1
b = a/(2**5)

db = 2*b*sqrt(1-b**2)
ddb = 2*db*sqrt(1-db**2)
dddb = 2*ddb*sqrt(1-ddb**2)
ddddb = 2*dddb*sqrt(1-dddb**2)
dddddb = 2*ddddb*sqrt(1-ddddb**2)

print(b)
print(db)
print(ddb)
print(dddb)
print(ddddb)
print(dddddb)

def Seno(a):
    b = a/(2**5)
    for i in range(5):
        db = 2**b*sqrt(1-b**2)
    return db
print(Seno(a))



