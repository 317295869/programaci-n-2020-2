#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
#
#
from math import sqrt, sin

def seno(alfa):    
    """
    Descripción general.
    Datos de entrada, ángulo al que se le quiere calcular el seno:
    
        alfa 
    
    Datos de salida, la aproximación al seno de alpha haciendo cinco 
    iteraciones:
        resultado
    """
    a = alfa
    b = a/(2**5)
    for i in range(5):
        b = 2*b*sqrt(1-b**2)
    
    '''
    Aquí el código que calcula una aproximación al
    seno de x basada en el documento Seno.pdf
    '''
    resultado = b
    return(resultado) 
    
if __name__ == "__main__":
    """ 
    Este bloque cuando utilizo este
    archivo como script, es decir, lo ejecuto con el
    icono del triangulo verde a la derecha en spyder
    o presionando la tecla F5
    """ 
    alfa = 0.5
    ra = seno(alfa)
    rf = sin(alfa)
    print("seno({:0.4}) = {:.4} "
          "# calculado con la implementación del algoritmo en el pdf\n"
          "sin({:.4})  = {:.4} "
          "# conculado con la finción sin del módulo math\n"
          "|seno({:0.4}) - sin({:.4})| = {:.6f} "
          "# cuatro digitos de precisión ".format(alfa,ra,alfa,rf,alfa,alfa,abs(ra-rf)))    
   print(_name_)
else:
   """
   Este bloque se ejecuta cuando utilizo este archivo
   como módulo, es decir, cuando en otro archivo escribo la 
   siguiente línea:

            from ASEFE_PBS_FuncSeno import seno
   o
            import ASEFE_PBS_FuncSeno
"""
   print(_name_)
   pass
