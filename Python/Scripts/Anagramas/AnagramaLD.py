#%% Luis Dario Ortíz Izquierdo

#Esta es la version original del programa Anagramas.py que hizo Luis Dario

#En esta parte le pide al usuario que ingrese dos cadenas
cadena1 = input('Escribe la primera cadena')
cadena2 = input('Escribe la segunda cadena')

##cadena1 = 'pé dro'
##cadena2 = 'POD.??ER'

#En esta parte los caracteres de las cadenas se hacen en minusculas
cadena1 = cadena1.lower()
cadena2 = cadena2.lower()

#En esta parte del programa se quitan los espacios que hay entre palabras
for i in cadena1:
    if i not in 'QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnmÁÉÚÍÓáéúíó':
        cadena1 = cadena1.replace(i,"")

for i in cadena2:
    if i not in 'QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnmÁÉÚÍÓáéúíó':
        cadena2 = cadena2.replace(i,"")

#%% Luis Dario Ortíz Izquierdo    
    

#En esta parte del programa se cambian las vocales con acento a vocales sin acento
cadena1 = cadena1.replace('á','a')
cadena1 = cadena1.replace('é','e')
cadena1 = cadena1.replace('í','i')
cadena1 = cadena1.replace('ó','o')
cadena1 = cadena1.replace('ú','u')

cadena2 = cadena2.replace('á','a')
cadena2 = cadena2.replace('é','e')
cadena2 = cadena2.replace('í','i')
cadena2 = cadena2.replace('ó','o')
cadena2 = cadena2.replace('ú','u')

#En esta parte del programa pide la condicion de que si la cadena 1 no tiene los 
#mismos caracteres que la cadena 2 diga que nos es un anagrama
r = False
cont = 0
for i in cadena1:
    if i in cadena2:
        cont += 1
    else:
        print('No es anagrama')
        r = True
        break

#En esta parte dek programa nos pide la condicion de que si la cadena 1 tiene los
#mismos caracteres que la cadena 2 diga que es un anagrama
if cont == len(cadena2):
    print('Sí es anagrama')
elif r != True:
    print('No es anagrama')



#----------------------------------------------------------------------


##cadena1 = 'Peedró'
##cadena2 = 'pod ,,é,E r'

#En esta parte del programa se hacen dos cadenas vacias
d1 = {}
d2 = {}

#En esta parte los caracteres de las cadenas se hacen en minusculas
cadena1 = cadena1.lower()
cadena2 = cadena2.lower()

#En esta parte del programa se quitan los espacios que hay entre palabras
for i in cadena1:
    if i not in 'QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnmÁÉÚÍÓáéúíó':
        cadena1 = cadena1.replace(i,"")

for i in cadena2:
    if i not in 'QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnmÁÉÚÍÓáéúíó':
        cadena2 = cadena2.replace(i,"")

#En esta parte del programa se cambian las vocales con acento a vocales sin acento
cadena1 = cadena1.replace('á','a')
cadena1 = cadena1.replace('é','e')
cadena1 = cadena1.replace('í','i')
cadena1 = cadena1.replace('ó','o')
cadena1 = cadena1.replace('ú','u')

cadena2 = cadena2.replace('á','a')
cadena2 = cadena2.replace('é','e')
cadena2 = cadena2.replace('í','i')
cadena2 = cadena2.replace('ó','o')
cadena2 = cadena2.replace('ú','u')

#En esta parte del programa pide la condicion de que si la cadena 1 no tiene los 
#mismos caracteres que la cadena 2 diga que nos es un anagrama, de lo contrario 
#nos diga que si es un anagrama
for i in cadena1:
    d1[i] = 0

for i in cadena1:
    d1[i] +=1

for i in cadena2:
    d2[i] = 0

for i in cadena2:
    d2[i] +=1

if d1 == d2:
    print('Sí es anagrama')
else:
    print('No es anagrama')

