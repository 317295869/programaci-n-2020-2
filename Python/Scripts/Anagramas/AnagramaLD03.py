#%% Luis Dario Ortíz Izquierdo

#Esta es la tercera modificacion del programa de Luis Dario que se hizo en clase
cadena1 = input('Escribe la primera cadena')
cadena2 = input('Escribe la segunda cadena')

##cadena1 = 'pé dro'
##cadena2 = 'POD.??ER'

#En esta parte los caracteres de las cadenas se hacen en minusculas
cadena1 = cadena1.lower()
cadena2 = cadena2.lower()



#%% Cambio de vocales con acento a vocales sin acento
#    y eliminación de espacios recorriendo la lista
#    vocal_con usando índices sobre la lista
vocal_con = ['á','é','í','ó','ú', " "]
vocal_sin = ['a','e','i','o','u',""]

n = len(vocal_con)
for i in range(n):
    cadena1 = cadena1.replace(vocal_con[i],vocal_sin[i])
    cadena2 = cadena2.replace(vocal_con[i],vocal_sin[i])

 
#%% Luis Dario Ortíz Izquierdo    

#En esta parte del programa pide la condicion de que si la cadena 1 no tiene los 
#mismos caracteres que la cadena 2 diga que nos es un anagrama, de lo contrario 
#nos diga que si es un anagrama
r = False
cont = 0
for i in cadena1:
    if i in cadena2:
        cont += 1
    else:
        print('No es anagrama')
        r = True
        break

if cont == len(cadena2):
    print('Sí es anagrama')
elif r != True:
    print('No es anagrama')



