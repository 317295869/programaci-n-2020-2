#%% Luis Dario Ortíz Izquierdo

#Esta es la novena y ultima modificacion del programa de Luis Dario que se hizo en clase
cadena1 = input('Escribe la primera cadena')
cadena2 = input('Escribe la segunda cadena')

##cadena1 = 'pé dro'
##cadena2 = 'POD.??ER'
#%% Función para cambio de vocales con acento a 
#   vocales sin acento y eliminación de espacios 
#   usando índices sobre la lista
def sin_acentos(cadena):
    vocal_con = ['á','é','í','ó','ú', " "]
    vocal_sin = ['a','e','i','o','u', ""]
    n = len(vocal_con)
    for i in range(n): # 0 1 2 3 4 5
        cadena = cadena.replace(vocal_con[i],vocal_sin[i])
    return cadena
"Ped ro"
"PÓder "

#Se hizo la funcion ordena los caracteres de las cadenas y los vuelve minusculas
def son_anagramas(cadena1,cadena2):
    return True if sorted( sin_acentos( cadena1.lower() ) ) == sorted( sin_acentos( cadena2.lower() ) ) else  False

#En esta parte del programa pide la condicion de que si la cadena 1 no tiene los 
#mismos caracteres que la cadena 2 diga que nos es un anagrama, de lo contrario 
#nos diga que si es un anagrama
print("Las cadenas son anagramas") if son_anagramas(cadena1,cadena2) else print("Las cadenas NO son anagramas")