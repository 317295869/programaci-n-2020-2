#%% Luis Dario Ortíz Izquierdo

#Esta es una modificacion del programa de Luis Dario
cadena1 = input('Escribe la primera cadena')
cadena2 = input('Escribe la segunda cadena')

##cadena1 = 'pé dro'
##cadena2 = 'POD.??ER'

#En esta parte los caracteres de las cadenas se hacen en minusculas
cadena1 = cadena1.lower()
cadena2 = cadena2.lower()

"""
for i in cadena1:
    if i not in 'QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnmÁÉÚÍÓáéúíó':
        cadena1 = cadena1.replace(i,"")

for i in cadena2:
    if i not in 'QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnmÁÉÚÍÓáéúíó':
        cadena2 = cadena2.replace(i,"")
"""
#%% Si una misma cadena se va a utilizar dos veces
#   es conveniente almacenarla en una variable 

caracteres_válidos = 'QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnmÁÉÚÍÓáéúíó'
for i in cadena1:
    if i not in caracteres_válidos:
        cadena1 = cadena1.replace(i,"")

for i in cadena2:
    if i not in caracteres_válidos:
        cadena2 = cadena2.replace(i,"")

#%% Cambio de vocales con acento a vocales sin acento
#    y eliminación de espacios recorriendo la lista
#    vocal_con elemento a elemento
vocal_con = ['á','é','í','ó','ú', " "]
vocal_sin = ['a','e','i','o','u',""]

for vocal in vocal_con:
    cadena1 = cadena1.replace(vocal,
                              vocal_sin[vocal_con.index(vocal)])
    cadena2 = cadena2.replace(vocal,
                              vocal_sin[vocal_con.index(vocal)])    
#%% Cambio de vocales con acento a vocales sin acento
#    y eliminación de espacios recorriendo la lista
#    vocal_con usando índices sobre la lista
n = len(vocal_con)
for i in range(n):
    cadena1 = cadena1.replace(vocal_con[i],vocal_sin[i])
    cadena2 = cadena2.replace(vocal_con[i],vocal_sin[i])

#%% Función para cambio de vocales con acento a 
#   vocales sin acento y eliminación de espacios 
#   usando índices sobre la lista
def sin_acentos(cadena):
    vocal_con = ['á','é','í','ó','ú', " "]
    vocal_sin = ['a','e','i','o','u', ""]
    n = len(vocal_con)
    for i in range(n): # 0 1 2 3 4 5
        cadena = cadena.replace(vocal_con[i],vocal_sin[i])
    return cadena
"Ped ro"
"PÓder "
"""
cadena1 = cadena1.lower() # "pedro"
cadena2 = cadena2.lower() # "poder"
cadena1 = sin_acentos(cadena1)
cadena2 = sin_acentos(cadena2)
cadena1 = sorted(cadena1) # "deopr"
cadena2 = sorted(cadena2) #  "deopr"
"""

#En esta parte del programa ordena los caracteres y nos dice que si la cadena 1 es 
#igual a la cadena 2 nos diga que son anagramas, de lo contrario nos dice que no lo son
cadena1 = sorted( sin_acentos( cadena1.lower() ) )
cadena2 = sorted( sin_acentos( cadena2.lower() ) )
if cadena1 == cadena2:
    print("Las cadenas son anagramas")
else:
    print("Las cadenas NO son anagramas")
    
#%% Luis Dario Ortíz Izquierdo    
    

#En esta parte del programa se cambian las vocales con acento a vocales sin acento
cadena1 = cadena1.replace('á','a')
cadena1 = cadena1.replace('é','e')
cadena1 = cadena1.replace('í','i')
cadena1 = cadena1.replace('ó','o')
cadena1 = cadena1.replace('ú','u')

cadena2 = cadena2.replace('á','a')
cadena2 = cadena2.replace('é','e')
cadena2 = cadena2.replace('í','i')
cadena2 = cadena2.replace('ó','o')
cadena2 = cadena2.replace('ú','u')

#En esta parte del programa pide la condicion de que si la cadena 1 no tiene los 
#mismos caracteres que la cadena 2 diga que nos es un anagrama, de lo contrario 
#nos diga que si es un anagrama
r = False
cont = 0
for i in cadena1:
    if i in cadena2:
        cont += 1
    else:
        print('No es anagrama')
        r = True
        break

if cont == len(cadena2):
    print('Sí es anagrama')
elif r != True:
    print('No es anagrama')