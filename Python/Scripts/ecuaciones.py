# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal.
"""
# Pérez Lazcano Ana Susana
"""
Método por Reducción
Para resolver este sistema de ecuaciones veamos una matriz A(x,y)=b,
donde también se puede ver como a*x+b*y=c y la seguda ecuacón tiene que ser 
d*x+f*y=e , de nuestros sistemas tenemos que renombrar las incognitas.
Para comparpadar que nuestro sistema tiene solución, nuestras incogitas deben
de ser diferentes de cero, asi nuestro sistema nos dara un determinante
Después Agergamos un codcional donde nos de los resultados de x 
y si el determinante es diferente de cero, podemos sustituirlo en la ecuación
multiplicando las ecuaciones por un -r donde r es un número real que se oucupa
para eliminar la incognita x
origial, así despejamos y obtenemos el valor de y.
Para resolver las dos matrices hacemos los procedimientos correspondietes.
"""
a=float (2)
b= float(5)
c= float (12)
d= float (3)
e= float (4)
f=float(11)
def redo (a ,b ,c , d, e, f):
   det=  a* e - b *d
   if det !=0:
    y= ( (c* d - f *a) / (b * d - e * a))
    x= ( (c - b * y) / a)
    return x, y 
   else:
       return "Esta ecuación no tiene solución"
   print(redo)

print(redo(2,3,4,1,10,12))
   
   # Valencia Dorantes Eduardo Alexis
"""
Metodo de sustitucion

Para resolver este problema veremos al nuestro sistema de ecuaciones
como una matriz de la forma A(x,y)=b, entonces para poder hacer eso, nuestra 
primera ecuación tiene la forma a*x+b*y=c y la segunda tiene la forma 
d*x+e*y=f, de lo anterior entonces cada constante de nuestros sistemas sera 
nombra por una incognita.
Ahora, para ver si nuestro sistema tiene solución, nuestro determinante debe
ser distinto de cero, así que hacemos una función que nos de la determinante.
Despues de ello tenemos que meter una condicional donde nos de los resultados
de x e y si el determinante es distinto de cero.
Solo faltaría hacer las funciones de x e y, así que de la matriz hacemos el
algebra correspondiente y ya solo faltaría pasar todo lo anterior a codigo.
"""

a = float(2)
b = float(3)
c = float(4)
d = float(1)
e = float(10)
f = float(12)

det = a * e - b * d

if det != 0 :
    x = (e * c - b * f) / det
    y = (a * f - d * c) / det
    print("La solucion al sistema es x= %f e y= %f" % (x, y))
else :
    m = d / a
    if m * c == f :
        print("El sistema tiene infinitas soluciones")
    else:
        print("El sistema no tiene soluciones")

"""
Con este programa se comprueba si realmente el resultado del programa anterior
es correcto lo cual debe ser cierto dado que fuimos cuidadosos al resolver el
programa, para este programa ocupamos el modulo Numpy para crear nuestras 
matrices y hacer su arreglo y sus soluciones
"""

import numpy as np

a = np.array([[2,3],[1,10]])    # Aquí hacemos la matriz A 
b = np.array([[4],[12]])        # Aqui hacemos la matriz b
x, y = np.linalg.solve(a,b)     # Aquí hacemos un llamado a las soluciones
print(x,y)                      # Imprimimos las soluciones

# Zavala Calderon Gerardo
"""
Aquí se en esta parte se resuelven los problemas a los dos metodos de 
ecuaciones restantes, donde la primera se resuelve como en el metedo de 
sustitucion, y el segundo tiene un procedimiento similar al metodo por 
reduccion.
"""

def Cramer(a,b,c,d,e,f):
    detA=(a*e-b*d)
    det1=(c*e-b*f)
    det2=(a*f-c*d)
    x=det1/detA
    y=det2/detA
    print('Las soluciones son:x=(%.4f),y=(%.4f)'%(x,y))

print(Cramer(2,3,4,1,10,12))

def igualacion(a,b,c,d,e,f):
    y=((a*f-d*c)/(a*e-d*b))
    x=(c-b*y)/a
    print('Las soluciones del sistema son:x=(%.4f), y=(%.4f)'%(x,y))

print(igualacion(2,3,4,1,10,12))
   

