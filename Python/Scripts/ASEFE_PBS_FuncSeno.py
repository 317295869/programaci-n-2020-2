2#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
#
#
 
from math import sqrt
from math import pi

def seno(alpha):    
    """
    Se presenta una forma de calcular el seno de un n ́umero basada en
    la publicaci ́on ”Algoritmos Sencillos para Evaluar Funciones Elementales”
    escrita por el profesor Pablo Barrera S anchez [PBS1996].
    Datos de entrada
    Alpha
    Datos de salida
    Beta
    """
   
    beta = alpha/pow(2,5)
    print("""
   Aproximación al seno de %f con B = %0.4f/2^5     

          Iteracion        Sin(2B)            

    """%(alpha,alpha))
    
    for i in list(range(5)):
        coseno_angulo=sqrt(1-pow(beta,2))
        seno_doble=2*beta*coseno_angulo
        beta=seno_doble
        print("""             %d             %0.7f   """%(i+1,beta))
    print("""\n   El valor de sin(%0.4f) = %0.7f aprox= Sin(2B)"""%(alpha,beta))
    print("""---------------------------------------------------------""")

lista=[0.1,0.5,1.5,pi]

for valor in lista:
    seno(valor)
    
    '''
    Aquí el código que calcula una aproximación al
    seno de x basada en el documento Seno.pdf
    '''
    
if __name__ == "__main__":
    seno(.5)
    print("Este bloque se ejecuta si el programa \
es llamado desde IDLE, la variable __name__ tiene \
almacenada la cadena '__main__' ")
    print(__name__)
else:
    print("Si el archivo se utiliza como modulo,\
 es decir se importa, la variable __name__ contiene\
el nombre nombre del archivo")
    print(__name__)
