#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 13:35:37 2020

@author: eduardo
"""
n = 5
i = 0
while True:
    respuesta = float(input("Escribe un valor floatante"))
    # 0,10
    b = 10
    """
    if respuesta > 0 and respuesta < b:
        print("El valor esta dentro del intervalo")
    else:
        print("El valor esta fuera del intervalo")
    """    
    if 0 < respuesta < b:
        print("El valor esta dentro dentro del intervalo")
    else:
        print("El valor esta fuera del intervalo")
    i = i + 1 # i += 1
    if i == n:
        break
   
""" # Ciclo infinito se detiene con break    
i = 0
while True:
    print("Programacion")
    i += 1 # i = i +1
    if i == 10:
        break # Termina el ciclo aqui
"""