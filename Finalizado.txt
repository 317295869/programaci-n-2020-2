Al fin puedo decir que termino el curso y con ello viene la evaluación, además este semestre para mí 
no fue tan ortodoxo y gran parte de este se realizo desde casa y en mi opinión no fue lo mismo y tuvo 
sus complicaciones.

Hablo de la materia, durante todo el curso siempre hubo un seguimiento de todo lo que lo que se vio 
desde la introducción hasta la última parte que fueron clases y herencia.
Pero hubo cosas que conforme avanzamos se fueron olvidando y otras que no quedaron tan claras, 
en mi caso la parte que se facilito fue desde la introducción hasta antes de ver clases ya que algunas 
cosas las tramos el semestre pasado en el Taller de Herramientas Computacionales por lo que no 
tuve ninguna complicación.

Por otra parte lo que se me complico fue la parte de Clases y Herencia dado de que no se vieron con 
tanta profundidad y se vio en las ultimas clases donde la mayoría estaba en evaluaciones y se necesito 
un poco más de tiempo para que quedara'más claro y no tuviera complicaciones a la hora de realizar 
los programas.

De ahí en fuera todo el curso fue de mi agrado y espero tomar Manejo de Datos con ustedes.
Muchas gracias por todo.