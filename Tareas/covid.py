#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  7 03:34:18 2020

@author: manuel
"""

# El modelo Kermack-McKendrick (SIR)
# cargar las bibliotecas numericas y de graficacion
import numpy as np
#import matplotlib
import matplotlib.pyplot as plt
# definicion del metodo de Euler
def km_model(init_vals, params, t):
    S_0, I_0, R_0 = init_vals
    S, I, R = [S_0], [I_0], [R_0]
    l, k = params
    deltat = t[2] - t[1] # Pasos de tiempo constantes
    for t_ in t[:-1]:
        next_S = S[-1] - (k * S[-1] * I[-1]) * deltat
        next_I = I[-1] + (k * S[-1] * I[-1] - l * I[-1]) * deltat
        next_R = R[-1] + (l * next_I) * deltat
        S.append(next_S)
        I.append(next_I)
        R.append(next_R)
    return np.stack([S,I,R]).T
# definicion de valores iniciales y parametros
# init_vals[S(0),I(0),R(0)]
init_vals = [9999, 1, 0]
params = [1e-1, 0.00002]
t_max = 300
deltat = 0.1
t = np.linspace(0, t_max, int(t_max/deltat))
km_results = km_model(init_vals, params, t)
# Escritura de valores a disco
outF = open("datos.txt", "w")
for line in km_results:
    print(line,file= outF)
outF.close()
print("Los valores de las poblaciones S,I,R, se han escrito en Disco")
print("Ver el archivo datos.text")
# Grafica de resultados
plt.figure(figsize=(8,6))
plt.plot(km_results, linewidth=3.3)
plt.legend(["S(t)-Susceptibles", "I(t)-Infectados", "R(t)-Recuperados"])
plt.xlabel("Tiempo",fontsize=18)
plt.ylabel("Población",fontsize=18)
plt.show()